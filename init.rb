require_relative 'lib/plateau'
require_relative 'lib/rover'

plateau = Plataeu.new

puts 'Enter coordinates:'
loop do
  plateau.x, plateau.y = gets.chomp.split(' ').map(&:to_i)
  break if plateau.valid?
  puts 'Invalid coordinates, try again'
end

rovers = Array.new(2) { Rover.new(plateau) }

rovers.each_with_index do |rover, index|
  loop
    puts "Enter rover#{index + 1} position"
    x, y, direction = gets.chomp.split(' ')
    rover.x = x.to_i
    rover.y = y.to_i
    rover.set_direction(direction)
    puts 'Enter explore instructions:'
    rover.instructions = gets.chomp
    break if rover.valid?
    puts 'Invalid data, try again'
  end
end

rovers.map(&:move)
