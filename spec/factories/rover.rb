FactoryGirl.define do
  factory :rover, class: Rover do
    plateau { build :plateau }
    initialize_with { new(plateau) } 
    x 1
    y 2
    instructions 'LMLMLMLMM'  
  end
end
