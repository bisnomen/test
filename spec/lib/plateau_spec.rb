require 'spec_helper'

RSpec.describe Plateau do
  subject { build :plateau }
  
  describe 'valid?' do
    context 'when valid' do 
      { x: 1, y: 1 }.each do |k, v|
        it "#{k}" do 
          subject.instance_variable_set("@#{k}".to_sym, v)
          expect(subject.valid?).to be_truthy
        end
      end
    end

    context 'when invalid' do 
      { x: 'a', y: 'a'}.each do |k, v|
        it "#{k}" do 
          subject.instance_variable_set("@#{k}".to_sym, v)
          expect(subject.valid?).to be_falsey
        end
      end
    end
  end
end
