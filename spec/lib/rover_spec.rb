require 'spec_helper'

RSpec.describe Rover do
  subject { build :rover }

  before { subject.set_direction('N') }
  let(:directions) { { 'N' => 0, 'E' => 90, 'S' => 180, 'W' => 270 } }

  describe '#move' do
    before { subject.move }

    it 'move rover to new direction' do 
      expect(subject.x).to eq 1
      expect(subject.y).to eq 3
      expect(subject.current_direction).to eq 'N'
    end
  end

  describe '#set_direction' do
    it 'set correct direction' do
      directions.each do |k,v|
        subject.set_direction(k)
        expect(subject.instance_variable_get(:@direction)).to eq v
      end
    end
  end

  describe 'valid?' do
    context 'when valid' do 
      { x: 1, y: 1, instructions: 'LLL' }.each do |k, v|
        it "#{k}" do 
          subject.instance_variable_set("@#{k}".to_sym, v)
          expect(subject.valid?).to be_truthy
        end
      end
    end

    context 'when invalid' do 
      { x: 'a', y: 'a', instructions: '' }.each do |k, v|
        it "#{k}" do 
          subject.instance_variable_set("@#{k}".to_sym, v)
          expect(subject.valid?).to be_falsey
        end
      end
    end
  end

  describe '#to_s' do 
    it 'should show currenct position' do 
      expect(subject.to_s).to eq '1 2 N'
    end
  end

  describe 'task' do 
    it 'rover 1 postion' do 
      subject.move 
      expect(subject.to_s).to eq '1 3 N'
    end

    context 'rover 2' do 
      before do 
        subject.x = 3
        subject.y = 3
        subject.set_direction('E')
        subject.instructions = 'MMRMMRMRRM'
      end

      it 'rover 2 positon' do 
        subject.move
        expect(subject.to_s).to eq '5 1 E'
      end
    end
  end
end
