class Rover
  DIRECTIONS = { 'N' => 0, 'E' => 90, 'S' => 180, 'W' => 270 }.freeze

  attr_accessor :x, :y, :instructions

  def initialize plateau
    @x = self.x
    @y = self.y
    @instructions = self.instructions
    @plateau = plateau
  end

  def move
    @instructions.split('').each do |i|
      case i
      when 'M'
        determine_position
        break if out_of_plateau?
      when 'L'
        left
      when 'R'
        right
      end
    end
  end

  def set_direction direction
    @direction = DIRECTIONS[direction]
  end

  def current_direction
    DIRECTIONS.key(@direction.abs)
  end

  def valid?
    @x.is_a?(Integer) && 
    @y.is_a?(Integer) && 
    DIRECTIONS.any? { |_, v| v == @direction } &&
    @instructions.size > 0
  end

  def to_s
    "#{@x} #{@y} #{current_direction}"
  end

  private

  def determine_position
    case @direction
      when 0, 360, -360
        @y += 1
      when 90, -270
        @x += 1
      when 180, -180
        @y -= 1
      when 270, -90
        @x -= 1
    end
  end

  def left
    @direction -= 90
    reset_if_360
  end

  def right
    @direction += 90
    reset_if_360
  end

  def reset_if_360
    @direction = 0 if @direction.abs == 360
  end

  def out_of_plateau?
    if @x > @plateau.x || @y > @plateau.y
      puts 'Rover stopped (out of plateau)'
      true
    else
      false
    end
  end
end
