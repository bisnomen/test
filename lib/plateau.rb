class Plateau
  attr_accessor :x, :y

  def valid?
    self.x.is_a?(Integer) && self.y.is_a?(Integer)
  end
end
